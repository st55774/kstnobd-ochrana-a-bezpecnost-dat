package cz.upce.fei.decipher;

import junit.framework.TestCase;
import org.junit.Assert;

public class TranspositionDecipherTest extends TestCase {
    private Decipher decipher = new TranspositionDecipher();

    public void testDecrypt() {
        final String message = "Cenoonommstmme oo snnio. s s c";
        final String expected = "Common sense is not so common.";
        final int key = 8;

        final String result = decipher.decrypt(key, message);

        Assert.assertEquals(expected, result);
    }
}