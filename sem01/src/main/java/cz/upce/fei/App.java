package cz.upce.fei;

import cz.upce.fei.cipher.CIPHER;
import cz.upce.fei.decipher.AffineDecipher;
import cz.upce.fei.decipher.CaesarDecipher;
import cz.upce.fei.decipher.BruteForceDecipher;
import cz.upce.fei.decipher.TranspositionDecipher;
import net.sf.extjwnl.JWNLException;

import java.util.Arrays;

/**
 * Main class.
 * */
public class App {
    /**
     * Default parameters length.
     * */
    private static final int PARAMETERS_LENGTH = 2;

    /**
     * Index o parameter cipher kind.
     * */
    private static final int CIPHER_KIND = 0;

    /**
     * Index o parameter message.
     * */
    private static final int CIPHER_MESSAGE= 1;

    public static void main(String[] args) throws JWNLException {
        if(args.length != 2){
            System.out.printf("Neplatný počet parametrů.\n[1] Musí obsahovat typ šifry:\n");
            Arrays.stream(CIPHER.values()).forEach(System.out::println);
            System.out.println("[2]Text zprávy");
            System.exit(1);
        }

        final CIPHER cipher = CIPHER.valueOf(args[CIPHER_KIND]);
        BruteForceDecipher decipher;

        switch (cipher){
            case CAESAR:
                decipher = new CaesarDecipher();
                decipher.bruteForce(args[CIPHER_MESSAGE]).forEach(System.out::println);
                break;
            case AFFINE:
                decipher = new AffineDecipher();
                decipher.bruteForce(args[CIPHER_MESSAGE]).forEach(System.out::println);
                break;
            case TRANSPOSITION:
                decipher = new TranspositionDecipher();
                decipher.bruteForce(args[CIPHER_MESSAGE]).forEach(System.out::println);
                break;
            default:
                System.out.println("Neplatná operace!");
                System.exit(2);
                break;
        }
    }
}
