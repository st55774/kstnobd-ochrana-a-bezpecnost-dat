package cz.upce.fei.cipher;

/**
 * Kind of cipher.
 *
 * */
public enum CIPHER {
    CAESAR,
    TRANSPOSITION,
    AFFINE;
}
