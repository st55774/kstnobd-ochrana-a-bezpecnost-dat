package cz.upce.fei.decipher;

/**
 * Simple decipher
 * */
public interface Decipher {
    /**
     * Using alphabet
     * */
    String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * Decrypt the message.
     *
     * @param key which is used to decrypt the message.
     * @param message message to decrypt.
     *
     * @return decrypted message.
     * */
    String decrypt(int key, String message);
}
