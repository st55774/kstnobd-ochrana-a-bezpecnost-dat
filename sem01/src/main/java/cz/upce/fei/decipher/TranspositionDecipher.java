package cz.upce.fei.decipher;

import net.sf.extjwnl.JWNLException;
import net.sf.extjwnl.data.POS;
import net.sf.extjwnl.dictionary.Dictionary;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public class TranspositionDecipher implements Decipher, BruteForceDecipher {
    @Override
    public Set<String> bruteForce(String message) throws JWNLException {
        Dictionary dictionary = Dictionary.getDefaultResourceInstance();
        Set<String> possibles = new HashSet<>();

        IntStream.range(1, message.length()).forEach(key -> {
            System.out.printf("Using the %d key...\n", key);

            String possible = decrypt(key, message);
            AtomicBoolean isEnglish = new AtomicBoolean(false);

            Arrays.stream(possible.split(" ")).forEach(word -> {
                try {
                    if(dictionary.getIndexWord(POS.NOUN, word) != null) isEnglish.set(true);
                } catch (JWNLException e) {
                    e.printStackTrace();
                }
            });

            if(isEnglish.get()) possibles.add(possible);
        });

        return possibles;
    }

    @Override
    public String decrypt(int key, String message) {
        int numberOfColumns = (int) Math.ceil((double)message.length() / key);
        int numberOfRows = key;
        int numberOfShadedBoxes = (numberOfColumns * numberOfRows) - message.length();

        String[] plaintext = new String[numberOfColumns];
        IntStream.range(0, plaintext.length).forEach(value -> plaintext[value] = "");

        int col = 0, row = 0;

        for (char symbol : message.toCharArray()){
            plaintext[col] += symbol;
            col++;

            if ((col == numberOfColumns) ||
                    (col == numberOfColumns - 1 && row >= numberOfRows - numberOfShadedBoxes)){
                col = 0;
                row++;
            }
        }

        StringBuilder result = new StringBuilder();
        Arrays.stream(plaintext).forEach(result::append);

        return result.toString();
    }
}
