package cz.upce.fei.decipher;

import net.sf.extjwnl.JWNLException;

import java.util.Set;

/**
 * Brute force decipher,
 * */
public interface BruteForceDecipher{
    /**
     * It will decprypt message using brute force.
     *
     * @return sets of possible messages.
     * */
    Set<String> bruteForce(String message) throws JWNLException;
}
