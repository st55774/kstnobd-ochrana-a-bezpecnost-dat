package cz.upce.fei.decipher;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class AffineDecipher implements BruteForceDecipher, Decipher{
    public static final String SYMBOLS = "\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\] ^_`abcdefghijklmnopqrstuvwxyz{|}~";

    @Override
    public Set<String> bruteForce(String message) {
        Set<String> possibles = new HashSet<>();

        int upper = (int) Math.pow(SYMBOLS.length(), 2);
        IntStream.range(0, upper).forEach(key -> {
            var keyA = getKeyParts(key)[0];
            if(gcd(keyA, SYMBOLS.length()) != 1)
                return;

            possibles.add(decrypt(key, message));
        });

        return possibles;
    }

    @Override
    public String decrypt(int key, String message) {
        var keys = getKeyParts(key);

        StringBuilder decrypted = new StringBuilder();
        var modInverseOfKeyA = findModInverse(keys[0], SYMBOLS.length());

        for (char c : message.toCharArray()) {
            if(SYMBOLS.contains(String.valueOf(c))){
                int symIndex = SYMBOLS.indexOf(c);
                decrypted.append((symIndex - keys[1]) * modInverseOfKeyA % SYMBOLS.length());
            } else {
                decrypted.append(c);
            }
        }

        return decrypted.toString();
    }

    protected int findModInverse(int a, int m){
        int u1 = 1, u2 = 0, u3 = a;
        int v1 = 0, v2 = 1, v3 = m;

        while (v3 != 0){
            int q = u3 / v3;
            v1 = (u1 - q * v1);
            v2 = (u2 - q * v2);
            v3 = (u3 - q * v3);

            u1 = v1;
            u2 = v2;
            u3 = v3;
        }

        return u1 % m;
    }

    protected int[] getKeyParts(int key){
        return new int[]{key / SYMBOLS.length(), key % SYMBOLS.length()};
    }

    /**
     * Return the GCD of a and b using Euclid's Algorithm
     * */
    protected int gcd(int key, int length){
         while(key != 0){
             key = length % key;
             if(key != 0) length = key;
         }

         return length;
    }
}
