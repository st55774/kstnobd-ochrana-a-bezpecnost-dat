package cz.upce.fei.decipher;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Decryptor of Caesar cipher.
 *
 * */
public class CaesarDecipher implements BruteForceDecipher {
    private static final int LOWER_ALPHABET = Decipher.ALPHABET.charAt(0);

    private static final int UPPER_ALPHABET = Decipher.ALPHABET.charAt(Decipher.ALPHABET.length()-1);

    @Override
    public Set<String> bruteForce(String message){
        Set<String> results = new HashSet<>();

        IntStream.range(1, Decipher.ALPHABET.length()).forEach(key -> {
            StringBuilder bruteForceMessage = new StringBuilder();
            for (char c : message.toCharArray()) {
                bruteForceMessage.append((c != ' ')? decryptLetter(c, key) : c);
            }
            results.add(bruteForceMessage.toString());
        });

        return results;
    }

    protected char decryptLetter(char c, int key){
        int next = c + key;

        if(next > UPPER_ALPHABET)
            next = (next - UPPER_ALPHABET) + LOWER_ALPHABET;

        char newLetter = (char) (next);

        return newLetter;
    }
}
